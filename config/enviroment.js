const dotenv = require('dotenv');

const config = Object.freeze({
	algolia: {
		aplicationId: process.env.APLICATION_ID,
		apiKey: process.env.API_KEY
	}
});

module.exports = config;
