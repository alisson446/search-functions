const admin = require('firebase-admin');
const functions = require('firebase-functions');
const algoliasearch = require('algoliasearch');

admin.initializeApp(functions.config().firebase);
const firebaseRef = admin.database().ref();
const client = algoliasearch("8EIV44941T", "bd8b97c25a975be6b0f7750b0a5079ba");


/*
* This function only create a index with all searchables atributes
*/
const setIndexSettings = (workspaceId, indexScope) => {
	const index = client.initIndex(`${workspaceId}_${indexScope}`);

	return new Promise((resolve, reject) => {
		index.setSettings({'searchableAttributes': []}, (err) => {
		  if (err) { reject(err) }
		  else { resolve(); } 
		});
	})
};


/*
* Firebase Functions
*/

exports.indexWorkspace = functions.database.ref('workspaces/{workspaceId}')
	.onWrite(event => {

		const workspace = event.data.val();
		const workspaceId = event.params.workspaceId;

		if (workspace != null && !event.data.previous.exists()) {

			return new Promise((resolve, reject) => {
				client.addApiKey(['search'], { 
					indexes: [
						`${workspaceId}_tasks`,
						`${workspaceId}_users`,
						`${workspaceId}_tags`,
						`${workspaceId}_teams`
					] 
				}, (err, content) => {
				  firebaseRef.child(`searchKeys/${workspaceId}/_global`).set(content['key'])
				  	.then(() => (
							Promise.all([
								setIndexSettings(workspaceId, 'tasks'),
								setIndexSettings(workspaceId, 'users'),
								setIndexSettings(workspaceId, 'tags'),
								setIndexSettings(workspaceId, 'teams'),
							])
						))
						.then(() => { resolve(); })
						.catch((err) => { reject(err); });
				});
			});

		} 

	});

exports.indexTask = functions.database.ref('tasks/{workspaceId}/{taskId}')
	.onWrite(event => {

		const task = event.data.val();

		if (task != null) {
			const index = client.initIndex(`${event.params.workspaceId}_tasks`);

			return new Promise((resolve, reject) => {
				index.saveObject({
					objectID: task.id,
					path: task.path,
					title: task.title
				}, (err, content) => {
					if (err) { reject(err); }
					else { resolve(content); }
				})
			});
		} 

	});

exports.indexUser = functions.database.ref('users/{workspaceId}/{userId}')
	.onWrite(event => {

		const user = event.data.val();

		if (user != null) {
			const index = client.initIndex(`${event.params.workspaceId}_users`);

			return new Promise((resolve, reject) => {
				index.saveObject({
					objectID: user.id,
					firstName: user.firstName,
					lastName: user.lastName
				}, (err, content) => {
					if (err) { reject(err); }
					else { resolve(content); }
				})
			});
		} 

	});

exports.indexTag = functions.database.ref('tags/{workspaceId}/{tagId}')
	.onWrite(event => {

		const tag = event.data.val();

		if (tag != null) {
			const index = client.initIndex(`${event.params.workspaceId}_tags`);

			return new Promise((resolve, reject) => {
				index.saveObject({
					objectID: tag.id,
					title: tag.title
				}, (err, content) => {
					if (err) { reject(err); }
					else { resolve(content); }
				})
			});
		} 

	});

exports.indexTeam = functions.database.ref('teams/{workspaceId}/{teamId}')
	.onWrite(event => {

		const team = event.data.val();

		if (team != null) {
			const index = client.initIndex(`${event.params.workspaceId}_teams`);

			return new Promise((resolve, reject) => {
				index.saveObject({
					objectID: team.id,
					title: team.title
				}, (err, content) => {
					if (err) { reject(err); }
					else { resolve(content); }
				})
			});
		} 

	});
